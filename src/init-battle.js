import Battle from "./Battle";
import ElectrikRobot from "./Robot/ElectrikRobot";
import FighterRobot from "./Robot/FighterRobot";
import Robot from "./Robot";
import SteelRobot from "./Robot/SteelRobot";
import shuffle from "./lib/shuffle";

import { isNilOrEmpty } from "ramda-adjunct";

const robots = [Robot, ElectrikRobot, SteelRobot, FighterRobot];

const addInputWarning = input =>
  isNilOrEmpty(input.value) && input.classList.add("warning");

export default (output, leftInput, rightInput) => event => {
  const beginButton = event.currentTarget;

  addInputWarning(leftInput);
  addInputWarning(rightInput);

  beginButton.disabled = true;

  const ClassLeftRobot = shuffle(robots)[0];
  const ClassRightRobot = shuffle(robots)[0];

  const leftRobot = new ClassLeftRobot(leftInput.value);
  const rightRobot = new ClassRightRobot(rightInput.value);
  const battle = new Battle(leftRobot, rightRobot, output);
  battle.start();

  beginButton.disabled = false;
};
