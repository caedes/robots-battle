import initBattle from "./init-battle";
import initInput from "./init-input";
import checkBeginButton from "./check-begin-button";

(function() {
  const leftInput = document.getElementById("left-input");
  const rightInput = document.getElementById("right-input");
  const beginButton = document.getElementById("begin-button");
  const output = document.getElementById("output");

  beginButton.addEventListener(
    "click",
    initBattle(output, leftInput, rightInput)
  );

  leftInput.addEventListener("focus", initInput);
  rightInput.addEventListener("focus", initInput);

  leftInput.addEventListener(
    "keyup",
    checkBeginButton(beginButton, rightInput)
  );
  rightInput.addEventListener(
    "keyup",
    checkBeginButton(beginButton, leftInput)
  );
})();
