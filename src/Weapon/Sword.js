import Weapon from ".";

export default class Sword extends Weapon {
  constructor() {
    super("sword");
  }
}
