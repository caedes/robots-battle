import Weapon from ".";

export default class Axe extends Weapon {
  constructor() {
    super("axe");
  }
}
