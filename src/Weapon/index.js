import createRandomValue from "../lib/createRandomValue";

export default class Weapon {
  constructor(name = "stick") {
    this.name = name;
    this.power = createRandomValue(15, 20);
    this.defense = createRandomValue(5, 8);
  }
}
