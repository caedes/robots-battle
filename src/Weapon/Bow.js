import Weapon from ".";

export default class Bow extends Weapon {
  constructor() {
    super("bow");
  }
}
