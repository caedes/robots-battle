import createRandomValue from "../lib/createRandomValue";
import Weapon from ".";

export default class Shield extends Weapon {
  constructor() {
    super("shield");

    this.power = 1;
    this.defense = createRandomValue(10, 15);
  }
}
