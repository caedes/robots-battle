export default (beginButton, otherInput) => event => {
  const currentInput = event.currentTarget;

  beginButton.disabled = currentInput.value === "" || otherInput.value === "";
};
