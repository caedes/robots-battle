export const VERSUS = "versus";
export const BEGIN = "begin";
export const ATTACK = "attack";
export const WINNER = "winner";

export default (type, payload = {}) => {
  return {
    type,
    payload
  };
};
