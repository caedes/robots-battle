import shuffle from "../lib/shuffle";
import createMessage, { VERSUS, BEGIN, ATTACK, WINNER } from "./create-message";

export default class Battle {
  constructor(leftRobot, rightRobot, output) {
    this.leftRobot = leftRobot;
    this.rightRobot = rightRobot;
    this.output = output;
    this.winner = null;
    this.messages = [
      createMessage(VERSUS, {
        leftRobot,
        rightRobot,
        leftRobotHp: leftRobot.hp,
        rightRobotHp: rightRobot.hp
      })
    ];
  }

  start() {
    const [firstRobot, secondRobot] = shuffle([
      this.leftRobot,
      this.rightRobot
    ]);

    this.messages.push(createMessage(BEGIN, { robot: firstRobot }));

    while (!this.winner) {
      firstRobot.attack(secondRobot);
      this.messages.push(
        createMessage(ATTACK, {
          attacker: firstRobot,
          defender: secondRobot,
          defenderHp: secondRobot.hp
        })
      );
      if (this.checkWinner()) break;
      secondRobot.attack(firstRobot);
      this.messages.push(
        createMessage(ATTACK, {
          attacker: secondRobot,
          defender: firstRobot,
          defenderHp: firstRobot.hp
        })
      );
      if (this.checkWinner()) break;
    }
  }

  concatMessages(outputMessage, currentMessage) {
    switch (currentMessage.type) {
      case VERSUS: {
        const {
          leftRobot,
          rightRobot,
          leftRobotHp,
          rightRobotHp
        } = currentMessage.payload;
        return `${outputMessage}\n${leftRobot.name} (${leftRobotHp}HP) VS ${rightRobot.name} (${rightRobotHp}HP)`;
      }

      case BEGIN: {
        const { robot } = currentMessage.payload;
        return `${outputMessage}\n${robot.name} will begin`;
      }

      case ATTACK: {
        const { attacker, defender, defenderHp } = currentMessage.payload;
        return `${outputMessage}\n${attacker.name} attack ${defender.name} with ${attacker.weapon.name} (${defenderHp}HP)`;
      }

      case WINNER: {
        const { robot } = currentMessage.payload;
        return `${outputMessage}\n${robot.name} win!`;
      }

      default:
        return outputMessage;
    }
  }

  checkWinner() {
    this.winner = this.rightRobot.isDead()
      ? this.leftRobot
      : this.leftRobot.isDead()
      ? this.rightRobot
      : null;

    if (this.winner) {
      this.messages.push(createMessage(WINNER, { robot: this.winner }));

      const outputMessage = this.messages.reduce(
        this.concatMessages,
        "Battle began!"
      );
      this.output.innerHTML = outputMessage;
    }

    return this.winner;
  }
}
