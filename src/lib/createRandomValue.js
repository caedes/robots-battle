export default (min, max) => {
  if (min === undefined || max === undefined) return Math.random();

  const minInt = Math.abs(min);
  const maxInt = Math.abs(max);

  return Math.floor(Math.random() * (maxInt - minInt) + minInt);
};
