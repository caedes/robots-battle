import Robot from ".";

export default class SteelRobot extends Robot {
  constructor(name) {
    super(name, "steel");
  }

  attack(robot) {
    super.attack(robot);
    if (robot.type === "fighter") robot.hp = Math.max(robot.hp - 5, 0);
  }
}
