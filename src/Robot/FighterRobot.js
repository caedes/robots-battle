import Robot from ".";

export default class FighterRobot extends Robot {
  constructor(name) {
    super(name, "fighter");
  }

  attack(robot) {
    super.attack(robot);
    if (robot.type === "normal" || robot.type === "electrik")
      robot.hp = Math.max(robot.hp - 5, 0);
  }
}
