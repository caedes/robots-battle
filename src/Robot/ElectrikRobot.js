import Robot from ".";

export default class ElectrikRobot extends Robot {
  constructor(name) {
    super(name, "electrik");
  }

  attack(robot) {
    super.attack(robot);
    if (robot.type === "steel") robot.hp = Math.max(robot.hp - 5, 0);
  }
}
