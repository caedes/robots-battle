import shuffle from "../lib/shuffle";
import createRandomValue from "../lib/createRandomValue";
import Weapon from "../Weapon";
import Sword from "../Weapon/Sword";
import Axe from "../Weapon/Axe";
import Shield from "../Weapon/Shield";
import Bow from "../Weapon/Bow";

const weapons = [Weapon, Sword, Axe, Shield, Bow];

export default class Robot {
  constructor(name = "Random", type = "normal") {
    this.name = name;
    this.hp = createRandomValue(90, 120);
    this.type = type;

    const ClassWeapon = shuffle(weapons)[0];
    this.weapon = new ClassWeapon();
  }

  attack(robot) {
    const attackPower = Math.max(this.weapon.power - robot.weapon.defense, 1);

    robot.hp = Math.max(robot.hp - attackPower, 0);
  }

  isDead() {
    return this.hp === 0;
  }
}
